#include <iostream>
using namespace std;
#include "Programa.h"
#include "Numeros.h"
#include "Lista.h"

class Programa {
    private:
        Lista *lista = NULL;
    public:
        Programa() {
            this->lista = new Lista();
        }
        Lista *get_lista() {
            return this->lista;
        }
};

int main (void) {
  int condicion = 1;
    Programa p = Programa();
    // Se crea la lista a partir de la clase
    Lista *lista = p.get_lista();
    // se pide que el usuario ingrese numeros para rellenar la lista
    for(int i=0;i<=condicion;i++){
      int numero;
      string opcion;
      cout << "Ingrese un numero: " << endl;
      cin >> numero;
      // se agrega el numero ingresado a la lista
      lista->crear(new Numeros(numero));
      // se imprime la lista
      lista->imprimir();
      // le piede si desea seguir ingresando numeros
      cout << "Ingrese una 's' si desea seguir ingresando numeros, 'n' sino: " << endl;
      cin >> opcion;
      // si opcion es igual a s se le suma uno mas a condicion y el for se va a repetir una vez mas
      if(opcion=="s"){
        condicion++;
      // sino se rompe el ciclo
      }else{
        break;
      }
    }
    return 0;
}
