# guia3_unidad1

*Ejercicio 1*                                                                                             

Crea listas con numeros de largo que quieras                                                                       

*Pre-requisitos* 

C++
make                                                                                                                    

*Problematica del Ejercicio*
se necesitaba crear una lista enlazada simple la cual se iba llenando de numeros, al ingresar un numero se muestra el contenido de la lista osea los numeros que ya a agregado si es que se agregaron

*Ejecutando*                                                                                                            
Si se ejecuta el programa le va a pedir un numero para ingresar a la lista, despues muentra el contenido de la lista, posteriormente se le pregunta al usuario se desea seguir ingresando numeros a la lista, si el usuario ingresa una 's' puede ingresar otro numero, si es una 'n' se cierra el programa

*Construido con*                                                                                                                                    
C++                                                                                                           

*Versionado*                                                                                                                                        
Version 0,1                                                                                                                                       

*Autor*                                                                                                                                           
Luis Rebolledo                                                                                                                                                                                                                                                                                                                        

