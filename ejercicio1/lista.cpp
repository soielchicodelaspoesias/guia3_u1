#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {}

// funcion para ingresar numeros
void Lista::crear (Numeros *numeros) {
    Nodo *tmp;

    // se crea un nuevo nodo
    tmp = new Nodo;
    tmp->numeros = numeros;
    tmp->sig = NULL;

    //si el es primer nodo agregado de la lista, lo deja como raíz y como último nodo
    if (this->raiz == NULL) {
        this->raiz = tmp;
        this->ultimo = this->raiz;
    // sino es el primer nodo agregado, apunta el actual último nodo al nuevo y deja el nuevo nodo como el último de la lista
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}

void Lista::imprimir () {
    // se crea una variable temporal
    Nodo *tmp = this->raiz;
    // se recorre la lista hasta que la variable temporal sea nulo
    cout << "Lista: " << endl;
    while (tmp != NULL) {
      // se imprime cada dato
      cout << " |" << tmp->numeros->get_numero() << "|";
      tmp = tmp->sig;
    }
    cout << endl;
}
