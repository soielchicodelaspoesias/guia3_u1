# guia3_unidad1

*Ejercicio 2*                                                                                             

Combina dos listas                                                                      

*Pre-requisitos* 

C++
make                                                                                                                                  
*Problematica del Ejercicio*
Se debe crear un programa que cree dos listas, llenadas por el usuario y posteriormente mostrar una tercera lista que contenga los datos de la lista uno mas los datos de la lista dos


*Ejecutando*                                                                                                                                         
Si se ejecuta el programa le va a pedir llenar numeris para llenar la lista 1, posteriormente se le pregunta al usuario se desea seguir ingresando numeros a la lista, si el usuario ingresa una 's' puede ingresar otro numero, si es una 'n' le pide al usuario llenar la lista dos, despues de ingresado un numero le pide la misma condicion anterior si ingresa una 's' puede ingresar otro numero, si es una 'n' imprime las tres listas y se cierra el programa


*Construido con*                                                                                                                                    
C++                                                                                                           

*Versionado*                                                                                                                                        
Version 0,1                                                                                                                                       

*Autor*                                                                                                                                           
Luis Rebolledo                                                                                                                                                                                                                                                                                                                        

