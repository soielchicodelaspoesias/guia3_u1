#include <iostream>
using namespace std;
#include "Programa.h"
#include "Numeros.h"
#include "Lista.h"

// se crea la clase que contien la lista
class Programa {
    private:
        Lista *lista = NULL;
    public:
        Programa() {
            this->lista = new Lista();
        }
        Lista *get_lista() {
            return this->lista;
        }
};

void llena_lista1(Lista *&lista1, Lista *&lista3){
  int condicion = 1;
  // Se llena la lista 1
  for(int i=0;i<=condicion;i++){
      int numero;
      string opcion;
      cout << "Llene la lista 1: ";
      cin >> numero;
      // el numero se agrega a la lista uno y a la tres que va a ser la combinacion de la lista1 y la lista2
      lista1->crear(new Numeros(numero));
      lista3->crear(new Numeros(numero));
      // se le pregunta si desea seguir llenando la lista 1
      cout << "Ingrese una 's' si desea seguir ingresando numeros, 'n' sino: " << endl;
      cin >> opcion;
      // si la opcion es s se le suma uno a condicion y el for se va a repetir
      if(opcion=="s"){
        condicion++;
        // sino se rompe el ciclo
      }else{
        break;
      }
    }
  }
void llena_lista2(Lista *&lista2, Lista *&lista3){
  int condicion = 1;
  // Se llena la lista 2
  for(int i=0;i<=condicion;i++){
      int numero;
      string opcion;
      cout << "Llene la lista 2: ";
      cin >> numero;
      // el numero se agrega a la lista2 y a la tres que va a ser la combinacion de la lista1 y la lista2
      lista2->crear(new Numeros(numero));
      lista3->crear(new Numeros(numero));
      cout << "Ingrese una 's' si desea seguir ingresando numeros, 'n' sino: " << endl;
      cin >> opcion;
      // si la opcion es s se le suma uno a condicion y el for se va a repetir
      if(opcion=="s"){
        condicion++;
        // sino se rompe el ciclo
      }else{
        break;
      }
    }
  }

int main (void) {
  int condicion = 1;
  Programa p = Programa();
  Programa p2 = Programa();
  Programa p3 = Programa();
  // se crean las tres listas distintas
  Lista *lista1 = p.get_lista();
  Lista *lista2 = p2.get_lista();
  Lista *lista3 = p3.get_lista();
  // se llama a las funciones
  llena_lista1(lista1, lista3);
  llena_lista2(lista2, lista3);
  // se imprimen las tres listas
  cout << "Lista 1: "<< endl;
  lista1->imprimir();
  cout << "Lista 2: "<< endl;
  lista2->imprimir();
  cout << "Lista 3: " << endl;
  lista3->imprimir();

    return 0;
}
