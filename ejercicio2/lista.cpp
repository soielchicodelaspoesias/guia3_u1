#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {}

void Lista::crear (Numeros *numeros) {
    Nodo *tmp;

    // se crea un nodo nuevo
    tmp = new Nodo;
    tmp->numeros = numeros;
    tmp->sig = NULL;

    //si el es primer nodo agregado de la lista, lo deja como raíz y como último nodo.
    if (this->raiz == NULL) {
        this->raiz = tmp;
        this->ultimo = this->raiz;
    // sino es el primero agregado, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}

void Lista::imprimir () {
    // se crea un nodo temporal
    Nodo *tmp = this->raiz;
    // recorre la lista hasta que tmp sea igual a  NULL
    while (tmp != NULL) {
      // imprime los valores de la lista
      cout << " |" << tmp->numeros->get_numero() << "|";
      tmp = tmp->sig;
    }
    cout << endl;
}
