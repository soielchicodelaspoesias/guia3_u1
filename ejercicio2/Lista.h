#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        Lista();
        void crear (Numeros *numeros);
        void imprimir ();
};
#endif
