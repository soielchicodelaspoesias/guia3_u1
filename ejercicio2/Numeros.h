#include <iostream>
using namespace std;

#ifndef NUMEROS_H
#define NUMEROS_H

class Numeros {
    private:
        int numero = 0;

    public:
        Numeros(int numero);
        int get_numero();
};
#endif
