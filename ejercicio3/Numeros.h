#include <iostream>
using namespace std;

#ifndef NUMEROS_H
#define NUMEROS_H

class Numeros {
    private:
        int numero = 0;

    public:
      void set_numero(int numero);
      Numeros(int numero);
      int get_numero();
};
#endif
