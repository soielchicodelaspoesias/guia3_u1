#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        Lista();
        void crear (Numeros *numeros);
        void recorrer(int &agregados);
        void llama_ordenar();
        void ordenar_menor_mayor(Nodo *inicio);
        void imprimir ();
};
#endif
