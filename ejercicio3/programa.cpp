#include <iostream>
using namespace std;
/* definición de la estructura Nodo. */
#include "Programa.h"
/* clases */
#include "Numeros.h"
#include "Lista.h"

class Programa {
    private:
        Lista *lista = NULL;
    public:
        /* constructor */
        Programa() {
            this->lista = new Lista();
        }
        Lista *get_lista() {
            return this->lista;
        }
};


int main (void) {
  int agregados = 0;
  int opcion;
  // se inicia la clase
  Programa p = Programa();
  // se crea la lista
  Lista *lista = p.get_lista();
    do {
      cout << "MENU" << endl;
      cout << "1.- Agregar numero" << endl;
      cout << "2.- Ver lista completada y ordenada" << endl;
      cout << "3.- Salir" << endl;

      cout << "Ingrese una opcion: ";
      cin >> opcion;

      switch(opcion){
        case 1:
            // se le ingresa un numero a la lista
            int number;
            cout << "inserte un numero: ";
            cin >> number;
            lista->crear(new Numeros(number));
            // agregados es el tamaño de la lista asi que se le suma uno
            agregados++;
            break;
        case 2:
            // imprime la lista
            cout << "lista original" << '\n';
            lista->imprimir();
            cout << "Lista ordenada de menor a mayor" << '\n';
            // se llama a la funcion que ordena de menor a mayor y se imprime la lista
            lista->llama_ordenar();
            lista->imprimir();
            cout << "lista completada" << '\n';
            // primero se agregan los numeros que faltan al final de la lista
            lista->recorrer(agregados);
            // se ordena de menor a mayor asi queda completada
            lista->llama_ordenar();
            // se imprime la lista
            lista->imprimir();
            break;
        }
     }while (opcion != 3);

    return 0;
}
