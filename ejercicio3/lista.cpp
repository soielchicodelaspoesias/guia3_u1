#include <iostream>
using namespace std;

#include "Programa.h"
#include "Lista.h"

Lista::Lista() {}

// funcion que ingresa numeros a la lista
void Lista::crear (Numeros *numeros) {
    Nodo *tmp;

    // se crea un nodo temporal
    tmp = new Nodo;
    // se le asigan el nmero
    tmp->numeros = numeros;
    tmp->sig = NULL;
    // si  se agrega el primer nodo lo deja como ultimo
    if (this->raiz == NULL) {
        this->raiz = tmp;
        this->ultimo = this->raiz;
    // si no es el primer nodo el actual apunta al nuevo y el nuevo al ultimo de la lista
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}

void Lista::ordenar_menor_mayor(Nodo *inicio){
  // se crea un nodo temporal
  Nodo *sgte;
  // variable temporal para guardar el numero
  int temp;
  // se recorre la lista
  while (inicio->sig !=NULL) {
    sgte = inicio -> sig;
    while (sgte != NULL){
      // si el primer numero es mayor al siguiente
      if(inicio->numeros->get_numero() > sgte->numeros->get_numero()){
        // se guarda el numero el la temporal
        temp = sgte -> numeros->get_numero();
        // el siguiente apunta al primero
        sgte->numeros->set_numero(inicio->numeros->get_numero());
        // y el primero apunta al siguiente
        inicio -> numeros->set_numero(temp);
      }
      sgte = sgte->sig;
    }
    inicio = inicio -> sig;
    sgte = inicio -> sig;
  }
}

void Lista::recorrer(int &agregados) {
  // se crean dos nodos temporales
  Nodo *tmp = this->raiz;
  Nodo *aux = this->raiz;
  // se recorre la lista
  for(int i=0;tmp!=NULL;i++){
    /* si la posicion i es distinta de agregados -1 se recorre esto es por que
    se salia de la lista cuando en le ultimo nodo apuntaba al siguiente*/
    if(i!=agregados-1){
      // se crea la variable primero que contiene el primer numero
      int primero = tmp->numeros->get_numero();
      aux->numeros->get_numero();
      aux = aux->sig;
      // segundo contiene el siguinte numeto
      int siguinte = aux->numeros->get_numero();
      // entonces si primero y segundo no son consecutivos
      if((primero + 1) != siguinte){
        // este for se recorre hasta que el primero y el seguno sean consecutivos
        for(int numeros=primero+1;numeros<siguinte;numeros++){
          // se agregan al final de la lista
          crear((new Numeros(numeros)));
          // y se le suman al agregados ya que es el tamaño de la lista
          agregados++;
          i++;
        }
      }
    }
    tmp = tmp->sig;
  }
}
void Lista::llama_ordenar() {
  // esta funcion recorre la lista con un nodo temporal y llama a ordenar
  Nodo *inicio= this ->raiz;
  while (inicio!=NULL) {
    ordenar_menor_mayor(inicio);
    inicio = inicio->sig;
  }
}

void Lista::imprimir () {
    //utiliza variable temporal para recorrer la lista.
    Nodo *tmp = this->raiz;
    // la recorre mientras sea distinto de NULL
    while (tmp!= NULL) {
      cout << " |" << tmp->numeros->get_numero() << "|";
      tmp = tmp->sig;
    }
    cout << endl;
}
